import { defineConfig } from 'vite'
import * as Path from 'path'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import AutoImport from 'unplugin-auto-import/vite'
import mkcert from 'vite-plugin-mkcert'
import optimizeAntd from './src/vite/plugins/vite-plugin-optimizeAntd'

const pathSrc = Path.resolve(__dirname, 'src')

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx({
      // options are passed on to @vue/babel-plugin-jsx
    }),
    AutoImport({
      // Auto import functions from Vue, e.g. ref, reactive, toRef...
      // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
      imports: ['vue'],

      dts: Path.resolve(pathSrc, 'auto-imports.d.ts'),
    }),
    mkcert(),
    optimizeAntd(),
  ],
  build: {
    rollupOptions: {
      output: {
        manualChunks: {
          // 将 Vue 相关库打包成单独的 chunk 中
          'vue-vendor': ['vue'],
          // 将 Lodash 库的代码单独打包
          lodash: ['lodash-es'],
          // 将组件库的代码打包
          library: ['ant-design-vue', 'echarts'],
        },
      },
    },
  },
  server: {
    port: 8080,
    https: true,
  },
  // optimizeDeps: {
  // 生产环境使用esbuild打包有bug不太行
  //   disabled: false,
  // },
  resolve: {
    alias: {
      '@': Path.join(__dirname, 'src'),
    },
  },
})
