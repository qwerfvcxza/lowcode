import { Module } from 'vuex'
import {
  AllComponentProps,
  textDefaultProps,
  imageDefaultProps,
} from 'cai-components'
import { insertAt } from '@/helper'

import { message } from 'ant-design-vue'
import store, { GlobalDataProps } from './index'
import { v4 as uuidv4 } from 'uuid'
import { cloneDeep } from 'lodash-es'
import { echartComponentProps } from '@/defaultProps'

type MoveDirection = 'Up' | 'Down' | 'Left' | 'Right'

export interface UpdateComponentData {
  key: keyof AllComponentProps | Array<keyof AllComponentProps>
  value: string | string[]
  id: string
  isRoot?: boolean
}
export interface HistoryProps {
  id: string
  componentId: string
  type: 'add' | 'delete' | 'modify'
  data: any
  // 删除后回滚往回插可以查到对应的位置
  index?: number
}
export interface EditorProps {
  components: ComponentData[]
  currentElement: string
  page: PageData
  copiedComponent?: ComponentData
  histories: HistoryProps[]
  historyIndex: number
  // 更新的缓存值
  cachedOldValues: any
  // 保存最多历史条目记录数
  maxHistoryNumber: number
}
export interface PageProps {
  backgroundColor: string
  backgroundImage: string
  backgroundRepeat: string
  backgroundSize: string
  height: string
  width: string
}

export type AllFormProps = PageProps & AllComponentProps & echartComponentProps

export interface ComponentData {
  // 这个元素的 属性，属性请详见下面
  props: Partial<AllComponentProps>
  // id，uuid v4 生成
  id: string
  // 业务组件库名称 l-text，l-image 等等
  name: 'l-text' | 'l-image'
  // 图层是否隐藏
  isHidden?: boolean
  // 图层是否锁定
  isLocked?: boolean
  // 图层名称
  layerName?: string
}

export const testComponents: ComponentData[] = [
  {
    id: uuidv4(),
    name: 'l-text',
    layerName: '图层1',
    props: {
      ...textDefaultProps,
      text: 'hello',
      fontSize: '20px',
      color: '#000000',
      lineHeight: '1',
      textAlign: 'left',
      fontFamily: '',
      height: '100px',
      width: '100px',
    },
  },
  // {
  //   id: uuidv4(),
  //   name: 'l-text',
  //   layerName: '图层2',
  //   props: {
  //     ...textDefaultProps,
  //     text: 'hello2',
  //     fontSize: '10px',
  //     fontWeight: 'bold',
  //     lineHeight: '2',
  //     textAlign: 'left',
  //     fontFamily: '',
  //   },
  // },
  // {
  //   id: uuidv4(),
  //   name: 'l-text',
  //   layerName: '图层3',
  //   props: {
  //     ...textDefaultProps,
  //     text: 'hello3',
  //     fontSize: '15px',
  //     // actionType: 'url',
  //     // url: 'https://www.baidu.com',
  //     lineHeight: '3',
  //     textAlign: 'left',
  //     fontFamily: '',
  //   },
  // },
]

export interface PageData {
  props: PageProps
  title: string
}

const pageDefaultProps = {
  backgroundColor: '#ffffff',
  backgroundImage: '',
  // 'url("https://static.imooc-lego.com/upload-files/%E5%B9%BC%E5%84%BF%E5%9B%AD%E8%83%8C%E6%99%AF%E5%9B%BE-994372.jpg")',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  height: '600px',
  width: '800px',
}

const modifyHistory = (
  state: EditorProps,
  history: HistoryProps,
  type: 'undo' | 'redo'
) => {
  const { componentId, data } = history
  const { key, oldValue, newValue } = data
  const newKey = key as keyof AllComponentProps | Array<keyof AllComponentProps>
  const updatedComponent = state.components.find(
    (component) => component.id === componentId
  )
  if (updatedComponent) {
    // check if key is array
    if (Array.isArray(newKey)) {
      newKey.forEach((keyName, index) => {
        updatedComponent.props[keyName] =
          type === 'undo' ? oldValue[index] : newValue[index]
      })
    } else {
      updatedComponent.props[newKey] = type === 'undo' ? oldValue : newValue
    }
  }
}

const debounceChange = (callback: (...args: any) => void, timeout = 1000) => {
  let timer = 0
  return (...args: any) => {
    console.log(timer)
    clearTimeout(timer)
    timer = window.setTimeout(() => {
      callback(...args)
    }, timeout)
  }
}

const pushHistory = (state: EditorProps, historyRecord: HistoryProps) => {
  // check historyIndex is already moved
  if (state.historyIndex !== -1) {
    // if moved, delete all the records greater than the index
    state.histories = state.histories.slice(0, state.historyIndex)
    // move historyIndex to unmoved
    state.historyIndex = -1
  }
  // check length
  if (state.histories.length < state.maxHistoryNumber) {
    state.histories.push(historyRecord)
  } else {
    // larger than max number
    // shift the first
    // push to last
    state.histories.shift()
    state.histories.push(historyRecord)
  }
}

const pushModifyHistory = (
  state: EditorProps,
  { key, value, id }: UpdateComponentData,
  oldValue: any
) => {
  pushHistory(state, {
    id: uuidv4(),
    componentId: id || state.currentElement,
    type: 'modify',
    data: { oldValue: state.cachedOldValues, newValue: value, key },
  })
  state.cachedOldValues = null
}
const pushHistoryDebounce = debounceChange(pushModifyHistory)

const editor: Module<EditorProps, GlobalDataProps> = {
  state: {
    components: testComponents,
    currentElement: '',
    page: {
      props: pageDefaultProps,
      title: 'test title',
    },
    histories: [],
    historyIndex: -1,
    cachedOldValues: null,
    maxHistoryNumber: 5,
  },
  mutations: {
    addComponent(state, component: ComponentData) {
      component.layerName = `图层${state.components.length + 1}`
      state.components.push(component)
      pushHistory(state, {
        id: uuidv4(),
        componentId: component.id,
        type: 'add',
        data: cloneDeep(component),
      })
    },
    setActive(state, currentId: string) {
      state.currentElement = currentId
    },
    undo(state) {
      // never undo before
      if (state.historyIndex === -1) {
        // undo the last item of the array
        state.historyIndex = state.histories.length - 1
      } else {
        // undo to the previous step
        state.historyIndex -= 1
      }
      // get the history record
      const history = state.histories[state.historyIndex]
      switch (history.type) {
        case 'add':
          // if create a component, we should remove it
          state.components = state.components.filter(
            (component) => component.id !== history.componentId
          )
          break
        case 'delete':
          // if delete a componet, we should restore it to the right position
          state.components = insertAt(
            state.components,
            history.index as number,
            history.data
          )
          break
        case 'modify': {
          modifyHistory(state, history, 'undo')
          break
        }
        default:
          break
      }
    },
    redo(state) {
      // can't redo when historyIndex is the last item or historyIndex is never moved
      if (state.historyIndex === -1) {
        return
      }
      // get the record
      const history = state.histories[state.historyIndex]
      // process the history data
      switch (history.type) {
        case 'add':
          state.components.push(history.data)
          // state.components = insertAt(state.components, history.index as number, history.data)
          break
        case 'delete':
          state.components = state.components.filter(
            (component) => component.id !== history.componentId
          )
          break
        case 'modify': {
          modifyHistory(state, history, 'redo')
          break
        }
        default:
          break
      }
      state.historyIndex += 1
    },
    copyComponent(state, id) {
      const currentComponent = store.getters.getElement(id)
      if (currentComponent) {
        state.copiedComponent = currentComponent
        message.success('已拷贝当前图层', 1)
      }
    },
    pasteCopiedComponent(state) {
      if (state.copiedComponent) {
        const clone = cloneDeep(state.copiedComponent)
        clone.id = uuidv4()
        clone.layerName += '副本'
        state.components.push(clone)
        pushHistory(state, {
          id: uuidv4(),
          componentId: clone.id,
          type: 'add',
          data: cloneDeep(clone),
        })
        message.success('已粘贴当前图层', 1)
      }
    },
    deleteComponent(state, id) {
      const currentComponent = store.getters.getElement(id)
      if (currentComponent) {
        const currentIndex = state.components.findIndex(
          (component) => component.id === id
        )
        state.components = state.components.filter(
          (component) => component.id !== id
        )
        pushHistory(state, {
          id: uuidv4(),
          componentId: currentComponent.id,
          type: 'delete',
          data: currentComponent,
          index: currentIndex,
        })
        message.success('已删除当前图层', 1)
      }
    },
    moveComponent(
      state,
      data: { direction: MoveDirection; amount: number; id: string }
    ) {
      const currentComponent = store.getters.getElement(data.id)
      if (currentComponent) {
        const oldTop = parseInt(currentComponent.props.top || '0', 0)
        const oldLeft = parseInt(currentComponent.props.left || '0', 0)
        const { direction, amount } = data
        switch (direction) {
          case 'Up': {
            const newValue = `${oldTop - amount}px`
            store.commit('updateComponent', {
              key: 'top',
              value: newValue,
              id: data.id,
            })
            break
          }
          case 'Down': {
            const newValue = `${oldTop + amount}px`
            store.commit('updateComponent', {
              key: 'top',
              value: newValue,
              id: data.id,
            })
            break
          }
          case 'Left': {
            const newValue = `${oldLeft - amount}px`
            store.commit('updateComponent', {
              key: 'left',
              value: newValue,
              id: data.id,
            })
            break
          }
          case 'Right': {
            const newValue = `${oldLeft + amount}px`
            store.commit('updateComponent', {
              key: 'left',
              value: newValue,
              id: data.id,
            })
            break
          }
          default:
            break
        }
      }
    },
    updateComponent(state, { key, value, id, isRoot }: UpdateComponentData) {
      // debugger
      const updateComponent = state.components.find(
        (component) => component.id === (id || state.currentElement)
      )
      if (updateComponent) {
        if (isRoot) {
          ;(updateComponent as any)[key as string] = value
        } else {
          const oldValue = Array.isArray(key)
            ? key.map((key) => updateComponent.props[key])
            : updateComponent.props[key]
          if (!state.cachedOldValues) {
            state.cachedOldValues = oldValue
          }
          pushHistoryDebounce(state, { key, value, id }, oldValue)
          if (Array.isArray(key) && Array.isArray(value)) {
            key.forEach((keyName, index) => {
              updateComponent.props[keyName] = value[index]
            })
          } else if (typeof key === 'string' && typeof value === 'string') {
            updateComponent.props[key] = value
          } else if (typeof key === 'string' && Array.isArray(value)) {
            updateComponent.props[key] = value as any
          }
        }
      }
    },
    updatePage(state, { key, value }) {
      state.page.props[key as keyof PageProps] = value
    },
  },
  getters: {
    getCurrentElement: (state) => {
      return state.components.find(
        (component) => component.id === state.currentElement
      )
    },
    getElement: (state) => (id: string) =>
      state.components.find(
        (component) => component.id === (id || state.currentElement)
      ),
    getComponentsLength: (state) => state.components.length,
    checkUndoDisable: (state) => {
      // 1 no history item
      // 2 move to the first item
      if (state.histories.length === 0 || state.historyIndex === 0) {
        return true
      }
      return false
    },
    checkRedoDisable: (state) => {
      // 1 no history item
      // 2 move to the last item
      // 3 never undo before
      if (
        state.histories.length === 0 ||
        state.historyIndex === state.histories.length ||
        state.historyIndex === -1
      ) {
        return true
      }
      return false
    },
  },
}

export default editor
