

export interface echartData {
    value: number
    name: string
  }
  
  export interface IEchartXAxisLabel {
    name: string
  }
  
  export interface IEchartValueData {
    value: any
  }
  
export interface radarData {
  name: String,
  max:number
}