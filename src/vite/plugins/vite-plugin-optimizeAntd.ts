import fs from 'fs'
import path from 'path'
import { Plugin } from 'vite'

const rootPath = path.resolve(__dirname, `./`)

function optimizeDeps(): Plugin {
  return {
    name: 'optimizeDeps',
    configResolved: (config) => {
      let concat = config.optimizeDeps.include?.concat(optimizeAntd())
      config.optimizeDeps.include = Array.from(new Set(concat))
    },
  }
}

function optimizeAntd(): string[] {
  let includes: string[] = ['ant-design-vue/es']
  const folders = fs.readdirSync(
    path.resolve(rootPath, `./node_modules/ant-design-vue/es`)
  )
  folders.map((name) => {
    const folderName = path.resolve(
      rootPath,
      `./node_modules/ant-design-vue/es`,
      name
    )
    let stat = fs.lstatSync(folderName)
    if (stat.isDirectory()) {
      let styleFolder = path.resolve(folderName, 'style')
      if (fs.existsSync(styleFolder)) {
        let stat = fs.lstatSync(styleFolder)
        if (stat.isDirectory()) {
          includes.push(`ant-design-vue/es/${name}/style`)
        }
      }
    }
  })

  return includes
}

export default optimizeDeps
