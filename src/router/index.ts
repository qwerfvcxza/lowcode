import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Editor from '../views/Editor.vue'
import TemplateDetail from '../views/TemplateDetail.vue'
import Index from '../views/Index.vue'
import login from '../views/login.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [ 
    {
      path: '/login',
      component:login
    },
    {
      path: '/',
      name: 'Index',
      component: Index,
      children: [
        { path: '', name: 'home', component: Home },
        { path: 'template/:id', name: 'template', component: TemplateDetail },
      ],
    },
    {
      path: '/editor',
      name: 'editor',
      component: Editor,
    },
  ],
})

export default router
