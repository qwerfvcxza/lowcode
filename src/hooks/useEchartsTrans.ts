import { echartData } from '../components/echart/types'

const useEchartsTrans = (
  data: echartData[],
  props: { xLabels: string[]; values: any[] }
): void => {
  watchEffect(() => {
    data.length = 0
    let maxLength =
      props.xLabels.length > props.values.length
        ? props.xLabels.length
        : props.values.length
    for (let i = 0; i < maxLength; i++) {
      data.push({ value: props.values[i], name: props.xLabels[i] })
    }
  })
}

export default useEchartsTrans
