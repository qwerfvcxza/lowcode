export interface UploadResp {
  code: number
  message: string
  data: {
    url: string
  }
}

export interface RespUploadData {
  errno: number
  message?: string
  data: {
    urls: string[]
  }
}
