import { useStore } from 'vuex'
import { GlobalDataProps } from '@/store'
import { KeyHandler, HotkeysEvent } from 'hotkeys-js'
import useHotKey from '../hooks/useHotKey'

const wrap = (callback: KeyHandler) => {
  const wrapperFn = (e: KeyboardEvent, event: HotkeysEvent) => {
    e.preventDefault()
    callback(e, event)
  }
  return wrapperFn
}

export default function initHotKeys() {
  const store = useStore<GlobalDataProps>()
  const currentId = computed(() => store.state.editor.currentElement)
  const undoIsDisabled = computed<boolean>(() => store.getters.checkUndoDisable)
  const redoIsDisabled = computed<boolean>(() => store.getters.checkRedoDisable)
  useHotKey('ctrl+c, command+c', () => {
    store.commit('copyComponent', currentId.value)
  })
  useHotKey('ctrl+v, command+v', () => {
    store.commit('pasteCopiedComponent')
  })
  useHotKey('backspace, delete', () => {
    store.commit('deleteComponent', currentId.value)
  })
  useHotKey('esc', () => {
    store.commit('setActive', '')
  })
  useHotKey(
    'up, w',
    wrap(() => {
      store.commit('moveComponent', {
        direction: 'Up',
        amount: 5,
        id: currentId.value,
      })
    })
  )
  useHotKey(
    'down, s',
    wrap(() => {
      store.commit('moveComponent', {
        direction: 'Down',
        amount: 5,
        id: currentId.value,
      })
    })
  )
  useHotKey(
    'right, d',
    wrap(() => {
      store.commit('moveComponent', {
        direction: 'Right',
        amount: 5,
        id: currentId.value,
      })
    })
  )
  useHotKey(
    'left, a',
    wrap(() => {
      store.commit('moveComponent', {
        direction: 'Left',
        amount: 5,
        id: currentId.value,
      })
    })
  )
  useHotKey('ctrl+z', () => {
    if (!undoIsDisabled.value) {
      store.commit('undo')
    }
  })
  useHotKey('ctrl+y', () => {
    if (!redoIsDisabled.value) {
      store.commit('redo')
    }
  })
}
