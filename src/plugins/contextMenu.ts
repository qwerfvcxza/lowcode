import { onMounted, onUnmounted } from 'vue'
import { useStore } from 'vuex'
import createContextMenu, { ActionItem } from '../components/createContextMenu'

const initContextMenu = () => {
  const store = useStore()
  const testActions: ActionItem[] = [
    {
      shortcut: 'Backspace',
      text: '删除图层',
      action: (cid) => {
        store.commit('deleteComponent', cid)
      },
    },
    {
      shortcut: 'Ctrl+C',
      text: '拷贝图层',
      action: (cid) => {
        store.commit('copyComponent', cid)
      },
    },
    {
      shortcut: 'Ctrl+V',
      text: '粘贴图层',
      action: (cid) => {
        store.commit('pasteCopiedComponent', cid)
      },
    },
  ]

  let destory: any
  onMounted(() => {
    destory = createContextMenu(testActions)
  })
  onUnmounted(() => {
    destory()
  })
}

export default initContextMenu
