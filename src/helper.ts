import { message } from 'ant-design-vue'
interface CheckCondition {
  format?: string[]
  // 使用多少 M 为单位
  size?: number
}
type ErrorType = 'size' | 'format' | null
export function beforeUploadCheck(file: File, condition: CheckCondition) {
  const { format, size } = condition
  const isValidFormat = format ? format.includes(file.type) : true
  const isValidSize = size ? file.size / 1024 / 1024 < size : true
  let error: ErrorType = null
  if (!isValidFormat) {
    error = 'format'
  }
  if (!isValidSize) {
    error = 'size'
  }
  return {
    passed: isValidFormat && isValidSize,
    error,
  }
}

export const commonUploadCheck = (file: File) => {
  const result = beforeUploadCheck(file, {
    format: ['image/jpeg', 'image/png'],
    size: 1,
  })
  const { passed, error } = result
  if (error === 'format') {
    message.error('上传图片只能是 JPG/PNG 格式!')
  }
  if (error === 'size') {
    message.error('上传图片大小不能超过 1Mb')
  }
  return passed
}

export const getImageDimensions = (url: string | File) => {
  return new Promise<{ width: number; height: number }>((resolve, reject) => {
    const img = new Image()
    img.src = typeof url === 'string' ? url : URL.createObjectURL(url)
    img.addEventListener('load', () => {
      const { naturalWidth: width, naturalHeight: height } = img
      resolve({ width, height })
    })
    img.addEventListener('error', () => {
      reject(new Error('There was some problem with the image.'))
    })
  })
}

export const getParentElement = (element: HTMLElement, className: string) => {
  let tmpEle = element
  while (tmpEle) {
    if (tmpEle.classList && tmpEle.classList.contains(className)) {
      return tmpEle
    }
    tmpEle = tmpEle.parentNode as HTMLElement
  }
  return null
}

export const insertAt = (arr: any[], index: number, newItem: any) => {
  return [...arr.slice(0, index), newItem, ...arr.slice(index)]
}

export function scaleTransform(width: number, height: number) {
  let scale1 = window.innerWidth / width

  let scale2 = window.innerHeight / height

  return scale1 < scale2 ? scale1 : scale2
}

export function replaceScale(data: string, scale: number) {
  return data.replace(/\"([^\"]+)px/g, (match, p1) => {
    return '"' + Number(p1) * scale + 'px'
  })
}

export function transform(data: string, width: number, height: number) {
  let scale = scaleTransform(width, height)

  return replaceScale(data, scale)
}

// console.log(
//   transform(
//     '[{"id":"18f87083-1f16-4b50-aab6-2e1ab4ee84e8","name":"l-text","layerName":"图层1","props":{"text":"hello","fontSize":"20px","fontFamily":"","fontWeight":"normal","fontStyle":"normal","textDecoration":"none","lineHeight":"1","textAlign":"left","color":"#000000","backgroundColor":"","actionType":"","url":"","height":"100px","width":"100px","paddingLeft":"0px","paddingRight":"0px","paddingTop":"0px","paddingBottom":"0px","borderStyle":"none","borderColor":"#000","borderWidth":"0","borderRadius":"0","boxShadow":"0 0 0 #000000","opacity":"1","position":"absolute","left":"0","top":"0","right":"0"}},{"name":"l-echart","id":"1b0618e9-84e4-45c8-b69b-94625d785b46","props":{"flag":"echart","tag":"bar-echart","title":"llll","width":"300px","height":"350px","values":[150,200,230,90,120,300,240],"xLabels":["xxxx","xxxxx","xxxx","xxxxx","xxx","xxx","xx"],"position":"absolute","borderStyle":"solid","left":"146.20001220703125px","top":"8px"},"layerName":"图层2"}]',
//     600,
//     800
//   )
// )
